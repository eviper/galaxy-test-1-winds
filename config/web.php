<?php


/** Site settings */
return [
    'name' => 'Base app',
    'controller' => 'site',
    'layout' => 'main',
    'language' => 'ru',
    'charset' => 'utf-8',
    'urlManager' => [

        'rules' => [
            'root' => 'site/index',

            'GET test/1' => 'site/test1',
            'GET test/2' => 'site/test2',
            'GET test/3' => 'site/test3',


        ]

    ],
    'db' => require(__DIR__ . '/db.php'),
];