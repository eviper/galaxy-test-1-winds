<?php

namespace components;

use components\config\GenerateConfig;

/**
 * Class GeneratorRandomArrayTree
 * @package components
 */
class GeneratorRandomArrayTree
{
    /** @var GenerateConfig $config */
    private $config;

    /** @var array $_array */
    private $_array = [];

    private $id = 0;

    /**
     * GeneratorRandomArrayTree constructor.
     * @param GenerateConfig $config
     */
    public function __construct(GenerateConfig $config)
    {
        $this->config = $config;
        $this->generate();
    }

    /**
     * @return array
     */
    public function getArray()
    {
        return $this->_array;
    }

    /**
     * @param int $deep
     * @param int $parent
     */
    private function generate($deep = 0, $parent = 0)
    {
        if ($deep < $this->config->deep) {
            $childSize = ($parent === 0) ? $this->getWidth() : rand(0, $this->config->child);
            for ($i = 0; $i < $childSize; $i++) {
                $this->id++;
                $this->_array[$this->id]['id'] = $this->id;
                $this->_array[$this->id]['parent'] = $parent;
                $this->_array[$this->id]['value'] = $this->genItem();
                $this->generate($deep + 1, $this->id);
            }
        }

        return;
    }


    /**
     * @return string
     */
    private function genItem()
    {
        $item = '';
        $size = strlen($this->config->characters) - 1;
        $itemSize = rand($this->config->char_min, $this->config->char_max);

        for ($i = 0; $i < $itemSize; $i++)
            $item .= $this->config->characters[rand(0, $size)];

        return $item;
    }

    /**
     * @return int
     */
    private function getWidth()
    {
        return $this->config->widthStatic ? $this->config->width : rand(1, $this->config->width);
    }


}