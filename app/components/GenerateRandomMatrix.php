<?php

namespace components;

use components\lib\CharacterInterface;

/**
 * Class GenerateRandomMatrix
 * @package components
 */
class GenerateRandomMatrix
{

    const MAX_DEEP = 4;
    const MAX_WIDTH = 4;

    /** @var int $maxDeep */
    private $maxDeep;

    /** @var int $maxWidth */
    private $maxWidth;

    /** @var  CharacterInterface $char */
    private $char;

    private $_array;

    /**
     * GenerateRandomMatrix constructor.
     * @param int $maxDeep
     * @param int $maxWidth
     * @param CharacterInterface $character
     */
    public function __construct(
        $maxDeep = self::MAX_DEEP,
        $maxWidth = self::MAX_WIDTH,
        CharacterInterface $character
    )
    {
        $this->maxDeep = $this->correct($maxDeep);
        $this->maxWidth = $this->correct($maxWidth);
        $this->char = $character;

        $this->generate();
    }

    /**
     * @return mixed
     */
    public function getArray()
    {
        return $this->_array;
    }

    /**
     * Generator
     */
    private function generate()
    {
        $result = [];
        $width = rand(1, $this->maxWidth);
        for ($i = 0; $i < $width; $i++) {
            $row = [];
            $char = $this->char->toChar();
            $deep = rand(1, $this->maxDeep);
            for ($k = 0; $k < $deep; $k++) {
                $row[] = $char . $k;
            }
            $result[] = $row;
        }
        $this->_array = $result;
    }


    /**
     * @param $attribute
     * @return mixed
     * @throws \Exception
     */
    private function correct($attribute)
    {

        if ($attribute && $this->notNegative($attribute))
            return $attribute;

        throw new \Exception('The maximum character can not be less than the minimum');
    }

    /**
     * @param $attribute
     * @return bool
     * @throws \Exception
     */
    private function notNegative($attribute)
    {
        if ($attribute > 0) {
            return true;
        }
        throw new \Exception('The ' . self::class . ' can not accept negative or zero values');
    }
}