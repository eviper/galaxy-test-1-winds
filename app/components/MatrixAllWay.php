<?php


namespace components;


class MatrixAllWay
{

    /** @var [] $_arrResult */
    private $_arrResult;


    /**
     * MatrixAllWay constructor.
     * @param array $array
     */
    public function __construct($array = [])
    {
        $this->allWay($array);
    }


    /**
     * @return array
     */
    public function getArray()
    {
        return $this->_arrResult;
    }

    /**
     * @param $arr
     * @param array $result
     */
    private function allWay($arr, $result = [])
    {
        if (!$arr) {
            $this->_arrResult[] = $result;
            return;
        }
        $row = array_shift($arr);
        $cache = $result;
        foreach ($row as $key => $item) {
            $result = $cache;
            $result[] = $item;
            $this->allWay($arr, $result);
        }
    }


}