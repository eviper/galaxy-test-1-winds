<?php

namespace components;

/**
 * CustomParser return parsing result.
 *
 * For example:
 *
 * ```php
 *
 * $customParser = new CustomParser($text, $pattern);
 * print_r($customParser->parseText());
 *
 * ```
 *
 * @property $pattern
 * @property $text
 *
 */
class CustomParser
{

    const DEFAULT_PATTERN = '#\[(\w+):?(.+)?]([\wа-яА-Я-—\s.«»(),]+)\[\/\1]#u';

    public $pattern;

    public $text;


    /**
     * CustomParser constructor.
     * @param string $text
     * @param string $pattern
     */
    public function __construct($text = '', $pattern = self::DEFAULT_PATTERN)
    {
        $this->pattern = $pattern;
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function parseText()
    {
        preg_match_all($this->pattern, $this->text, $matches, PREG_SET_ORDER);
        return $matches;
    }

}