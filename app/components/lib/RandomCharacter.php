<?php

namespace components\lib;

/**
 * Class RandomCharacter
 * @package components\lib
 */
class RandomCharacter implements CharacterInterface
{
    const CHARACTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    const MAX_NUMBER = 7311616;

    private $maxNumber;

    public function __construct($maxNumber = 1)
    {
        $this->maxNumber = $maxNumber;
    }

    /**
     * @return string
     */
    public function toChar()
    {
        $item = '';
        $digit = $digit = rand(1, $this->maxNumber);
        $len = strlen(self::CHARACTERS);
        $module = $digit % $len;
        $digit -= $module;
        while ($digit > $len) {
            $digit = intdiv($digit, $len);
            if ($digit > $len) {
                $moduleDigit = $digit % $len;
                $digit -= $moduleDigit;
                $item .= self::CHARACTERS[$moduleDigit - 1];
            } else if ($digit < $len)
                $item .= self::CHARACTERS[$digit - 1];
            else if ($digit === $len) {
                $item .= self::CHARACTERS[$digit - 1];
                break;
            }
        }
        if ($module > 0) {
            $item .= self::CHARACTERS[$module - 1];
        }

        return $item;
    }
}