<?php

namespace components\lib;

/**
 * Class DefaultCharacter
 * @package components\lib
 */
class DefaultCharacter implements CharacterInterface
{
    const CHARACTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    private $count;

    /**
     * @return string
     */
    public function toChar()
    {
        $item = '';
        $digit = $this->count++;
        $len = strlen(self::CHARACTERS);
        $module = $digit % $len;
        $digit -= $module;
        while ($digit > $len) {
            $digit = intdiv($digit, $len);
            if ($digit > $len) {
                $moduleDigit = $digit % $len;
                $digit -= $moduleDigit;
                $item .= self::CHARACTERS[$moduleDigit - 1];
            } else if ($digit < $len)
                $item .= self::CHARACTERS[$digit - 1];
            else if ($digit === $len) {
                $item .= self::CHARACTERS[$digit - 1];
                break;
            }
        }
        if ($module > 0) {
            $item .= self::CHARACTERS[$module - 1];
        }

        return $item;
    }
}