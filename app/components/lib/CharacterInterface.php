<?php

namespace components\lib;

/**
 * Interface CharacterInterface
 * @package components\lib
 */
interface CharacterInterface
{
    public function toChar();
}