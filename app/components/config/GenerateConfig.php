<?php

namespace components\config;


/**
 * Class GenerateConfig
 * @package components
 *
 * @property int $deep
 * @property int $width
 * @property int $char_max
 * @property int $char_min
 * @property int $child
 * @property string $characters
 * @property boolean $widthStatic
 */
class GenerateConfig
{
    const CHARACTERS_MAX = 4;
    const CHARACTERS_MIN = 1;
    const ARRAY_DEEP = 5;
    const ARRAY_WIDTH = 5;
    const CHILD_AMOUNT = 3;
    const CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const WIDTH_STATIC = false;

    private $config = [
        'deep' => self::ARRAY_DEEP,
        'width' => self::ARRAY_WIDTH,
        'char_max' => self::CHARACTERS_MAX,
        'char_min' => self::CHARACTERS_MIN,
        'child' => self::CHILD_AMOUNT,
        'characters' => self::CHARACTERS,
        'widthStatic' => self::WIDTH_STATIC,
    ];

    /**
     * GenerateConfig constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct($config = [])
    {
        if (isset($config['deep']))
            $this->deep = $config['deep'];
        if (isset($config['width']))
            $this->width = $config['width'];
        if (isset($config['char_max']))
            $this->char_max = $config['char_max'];
        if (isset($config['char_min']))
            $this->char_min = $config['char_min'];
        if (isset($config['child']))
            $this->child = $config['child'];
        if (isset($config['widthStatic']))
            $this->widthStatic = $config['widthStatic'];
        $this->correct($this->config);
    }


    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->config)) {
            return $this->config[$name];
        }
        throw new \Exception(self::class . ' has\'nt property ' . $name);
    }

    /**
     * @param $name
     * @param $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        if ($value && array_key_exists($name, $this->config)) {
            $this->config[$name] = $value;
            return;
        }
        throw new \Exception(self::class . ' not support this property ' . $name);
    }

    /**
     * @param $config
     * @return array
     * @throws \Exception
     */
    private function correct($config)
    {
        foreach ($config as $key => $attribute) {
            if ($key != 'characters' && $key != 'widthStatic')
                $this->notNegative($attribute);
        }
        if ($config['char_min'] <= $config['char_max'])
            return $config;
        throw new \Exception('The maximum character can not be less than the minimum');
    }

    /**
     * @param $attribute
     * @return bool
     * @throws \Exception
     */
    private function notNegative($attribute)
    {
        if ($attribute > 0) {
            return true;
        }
        throw new \Exception('The ' . self::class . ' can not accept negative or zero values');
    }

}