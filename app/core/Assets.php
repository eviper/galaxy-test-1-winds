<?php

namespace core;

/**
 * Class Assets
 * @package base
 */
Trait Assets
{
    /**
     * @param $file
     */
    public function js($file)
    {
        echo '<script src="' . ASSETS . 'js/' . $file . '?version=' . uniqid() . '"></script>';
    }

    /**
     * @param $file
     */
    public function css($file)
    {
        echo '<link rel="stylesheet" href="' . ASSETS . 'css/' . $file . '?version=' . uniqid() . '">';
    }

}