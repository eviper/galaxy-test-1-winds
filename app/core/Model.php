<?php

namespace core;

use core\db\Db;

/**
 * Class Model
 * @package base
 *
 * @property $errors []
 */
class Model extends Object
{
    /** Table name in SQL */
    const TABLE = '';

    /** @var array $errors */
    public $errors = [];


    public function rules()
    {
        return [];
    }

    /**
     * Create new Db record
     *
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $valuesArray = [];
        $columnsArray = [];

        foreach ($this as $param => $value) {
            if (!in_array($param, static::properties())) {
                continue;
            }

            $columnsArray[] = $param;
            $valuesArray[':' . $param] = $value;
        }

        /** @var string $columns Looks like (colm1, colm2, ..) */
        $columns = '(' . implode(',', $columnsArray) . ')';
        /** @var string $values Looks like (:colm1, :colm2, ..) */
        $values = '(' . implode(',', array_keys($valuesArray)) . ')';

        $sql = 'INSERT INTO ' . static::tableName() . $columns . ' VALUES ' . $values . ';';

        return Db::execute($sql, $valuesArray);
    }


    /**
     * Find current Model by param
     *
     * @param array $params
     * @return bool|mixed
     * @throws \Exception
     */
    public static function findBy(array $params = [])
    {
        static::checkParams($params);
        $whereArray = [];
        $valuesArray = [];

        foreach ($params as $param => $value) {
            $whereArray[] = $param . ' = ' . ':' . $param;
            $valuesArray[':' . $param] = $value;
        }
        /** @var string $where Looks like  colm1=:col1 AND colm2=:col2 AND .. */
        $where = implode(' AND ', $whereArray);

        $sql = 'SELECT * FROM ' . static::tableName() . ' WHERE ' . $where . '  LIMIT 1;';

        return Db::getModel($sql, $valuesArray, static::className());
    }

    /**
     * Find current Model by id
     *
     * @param $id
     * @return bool|mixed
     * @throws \Exception
     */
    public static function findById($id)
    {
        return static::findBy(['id' => $id]);
    }

    /**
     * Delete current Db record
     */
    public function delete()
    {
        $sql = ' DELETE FROM ' . static::tableName() . ' WHERE id = ' . $this->id . ';';
        return Db::remove($sql);
    }

    /**
     * @param $attributes
     */
    public function setAttr($attributes)
    {
        foreach ($attributes as $attribute => $value) {
            $this->$attribute = $value;
        }
    }

    /**
     * @param array $params
     * @throws \Exception
     */
    protected static function checkParams(array $params)
    {
        if (empty($params)) {
            new \Exception('Params is empty. Class:' . static::className());
        }
        foreach (array_keys($params) as $param) {
            if (!in_array($param, static::properties())) {
                new \Exception('Param - ' . $param . ' not declared in class ' . static::className());
            }
        }
    }

    /**
     * @return string
     */
    private static function tableName()
    {
        if (empty(static::TABLE)) {
            new \Exception('Table name missed. Class: ' . static::className());
        }
        return static::TABLE;
    }

    /**
     * @param $property
     * @return bool
     * @throws \Exception
     */
    public function validateProperty($property)
    {
        $rules = $this->rules();
        if (empty($rules)) {
            throw new \Exception('This model hasnt this field: ' . $property);
        }
        $pattern = $rules[$property];

        if (preg_match($pattern, $this->$property)) {
            return true;
        }
        $this->errors[$property] = 'This field - ' . $property . ' not valid';
        return false;
    }


    /**
     * @return bool
     */
    public function load()
    {
        $className = static::classCurrentName();

        if (isset(App::$app->router->property[$className])) {
            foreach (App::$app->router->property[$className] as $property => $value) {
                $this->$property = $value;
            }
            return true;
        }
        return false;
    }


}