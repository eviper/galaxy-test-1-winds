<?php

namespace core;

class Router
{
    const DEFAULT_ACTION = 'actionIndex';
    const ERROR_PATH = PUBLIC_PATH . '/errors/';

    /** @var $controller string Current controller */
    public $controller;

    /** @var $action string Current action */
    public $action;

    /** @var $controller string default controller */
    public $defaultController = 'Site';

    /** @var $rules_list array */
    private $rules_list = [];

    /** @var array $property */
    public $property = [];

    /** @var string $webRoot */
    private $webRoot;

    /**
     * Assigns a controller and action
     *
     */
    /** TODO rebuild parsing. Need make it more flexible  */
    public function parsing()
    {
        $ruleArr = [];
        $urn = parse_url($_SERVER['REQUEST_URI']);
        $rules = $this->rules_list[$_SERVER['REQUEST_METHOD']];

        if ($urn['path'] == $this->webRoot && $_SERVER['REQUEST_METHOD'] === 'GET') {
            if (isset($rules['root'])) {
                $ruleArr = explode("/", $rules['root']);
            } else {
                $this->setDefault();
            }
        } else {
            foreach ($rules as $code => $rule) {

                if ($code != 'root' && preg_match($code, $urn['path'])) {
                    $ruleArr = explode("/", $rule);
                    break;
                }
            }
        }


        unset($this->rules_list);


        if (empty($ruleArr) && !isset($this->controller)) {
            throw new \Exception('Not found rule: path - ' . $urn['path']);
        }

        if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($urn['query']))
            parse_str($urn['query'], $this->property);
        elseif ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST)) {
            $this->property = $_POST;
        }


        if (!$this->controller)
            $this->controller = ucfirst($ruleArr[0]);
        if (!$this->action)
            $this->action = ucfirst($ruleArr[1]);


        return true;
    }

    /**
     * Create routing map
     *
     * @param array $urlManager
     * @return bool
     * @throws \Exception
     */
    public function add($urlManager = [])
    {
        $this->webRoot = isset($urlManager['webRoot']) ? $urlManager['webRoot'] : '/';

        if (isset($urlManager['rules'])) {
            foreach ($urlManager['rules'] as $map => $path) {
                $method = strtoupper(explode(" ", $map)[0]);

                if ($method != 'POST' && $method != 'PUT' && $method != 'DELETE') {
                    $method = 'GET';
                }
                if (!$rule = trim(str_replace($method, '', $map))) {
                    $rule = '/';
                }
                if ($rule != 'root') {
                    if (stristr($rule, '<:id>')) {
                        $rule = str_replace('/<:id>', '', $rule);
                        $rule = '#' . $rule . '/[0-9]+#';
                    } else {
                        $rule = '#' . $rule . '#';
                    }
                }
                $this->rules_list[$method][$rule] = $path;
            }
            return true;
        }

        throw new \Exception('Route not found in config file');
    }

    /**
     * @param string $code
     * @param string $path
     */
    public function errorPage($code, $path = '')
    {
        include_once (empty($path) ? self::ERROR_PATH : $path) . $code . PHP_E;
    }


    /**
     * Prepare default page
     */
    private function setDefault()
    {
        $this->controller = $this->defaultController;
        $this->action = self::DEFAULT_ACTION;
    }
}