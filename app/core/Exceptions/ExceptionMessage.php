<?php

namespace core\Exceptions;

use Exception;

/**
 * Class ExceptionMessage
 * @package core\Exceptions
 */
class ExceptionMessage extends \Exception
{

    const VALIDATION_WRONG_FORMAT = 0;
    const ERROR_MESSAGE = 1;
    const INFO_MESSAGE = 2;


    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function displayMessage()
    {
        echo '<div class="' . $this->getClass($this->getCode()) . '">' . $this->getMessage() . '</div>';
    }

    private function getClass($code)
    {
        $class = [
            self::VALIDATION_WRONG_FORMAT => 'validation_error',
            self::ERROR_MESSAGE => 'error_message',
            self::INFO_MESSAGE => 'info_message'
        ];

        return $class[$code];
    }

}