<?php

namespace core\Exceptions;

use core\App;
use Exception;

class ErrorHandler
{
    static public function getErrorName($error)
    {
        $errors = [
            E_ERROR => 'ERROR',
            E_WARNING => 'WARNING',
            E_PARSE => 'PARSE',
            E_NOTICE => 'NOTICE',
            E_CORE_ERROR => 'CORE_ERROR',
            E_CORE_WARNING => 'CORE_WARNING',
            E_COMPILE_ERROR => 'COMPILE_ERROR',
            E_COMPILE_WARNING => 'COMPILE_WARNING',
            E_USER_ERROR => 'USER_ERROR',
            E_USER_WARNING => 'USER_WARNING',
            E_USER_NOTICE => 'USER_NOTICE',
            E_STRICT => 'STRICT',
            E_RECOVERABLE_ERROR => 'RECOVERABLE_ERROR',
            E_DEPRECATED => 'DEPRECATED',
            E_USER_DEPRECATED => 'USER_DEPRECATED',
        ];
        if (array_key_exists($error, $errors)) {
            return $errors[$error] . " [$error]";
        }
        return $error;
    }

    public function register()
    {
        if (DEBUG) {
            ini_set('display_errors', 'on');
            error_reporting(E_ALL | E_STRICT);
        }
        set_error_handler([$this, 'errorHandler']);
        set_exception_handler([$this, 'exceptionHandler']);
        register_shutdown_function([$this, 'fatalErrorHandler']);
    }

    /**
     * @param $errNumber
     * @param $errStr
     * @param $file
     * @param $line
     * @return bool
     */
    public function errorHandler($errNumber, $errStr, $file, $line)
    {
        $this->showError($errNumber, $errStr, $file, $line);
        return true;
    }

    /**
     * @param Exception $e
     */
    public function exceptionHandler($e)
    {
        $this->showError(get_class($e), $e->getMessage(), $e->getFile(), $e->getLine(), 404);
    }

    /**
     * Return fatal Error
     */
    public function fatalErrorHandler()
    {
        if ($error = error_get_last() AND $error['type'] & (E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR)) {
            ob_end_clean();
            $this->showError($error['type'], $error['message'], $error['file'], $error['line'], 500);
        }
    }

    /**
     * @param $errNumber
     * @param $errStr
     * @param $file
     * @param $line
     * @param int $status
     */
    public function showError($errNumber, $errStr, $file, $line, $status = 500)
    {

        header("HTTP/1.1 $status");


        if (!DEBUG && ($status == 500 || $status == 404)) {
            App::$app->router->errorPage($status);
            return;
        }

        echo '<div style="clear: both"></div><b>' . self::getErrorName($errNumber) . "</b><hr /> <b>Error:</b> " . $errStr . '<hr /> <b>File:</b> ' . $file . '. <b>Line:</b> ' . $line . '<hr />';
    }

}