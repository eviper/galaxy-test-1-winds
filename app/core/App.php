<?php

namespace core;

use core\Exceptions\ErrorHandler;
use core\db\Connect;

/**
 * Class App
 * @package core
 */
class App extends Object implements AppInterface
{
    public static $app;

    /** @var  $charset string */
    public $charset = 'utf-8';

    /** @var  $language string */
    public $language = 'en';

    /** @var $router Router */
    public $router;

    /** @var $name string App name */
    private $name = '';

    /** @var $layout string default layout */
    private $layout = 'Main';

    /** @var array dataBase config */
    private $db = [];

    /** @var array $configs Other settings */
    private $configs = [];

    /**
     * App constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct($config = [])
    {
        include 'define.php';
        self::$app = $this;

        (new ErrorHandler)->register();
        /** Errors Handler */
        $this->router = new Router;
        /** Router */
        $this->init($config);
    }

    /**
     * @param $config
     * @throws \Exception
     */
    public function init(&$config)
    {
        /** Set db */
        if (isset($config['db']) && is_array($config['db'])) {
            $this->db = $config['db'];
            unset($config['db']);
        } else {
            throw new \Exception('Not found db config ');
        }


        /** Set app name */
        if (isset($config['name'])) {
            $this->name = $config['name'];
            unset($config['name']);
        }

        /** Set default controller */
        if (isset($config['controller'])) {
            $this->router->defaultController = ucfirst($config['controller']);
            unset($config['controller']);
        }

        /** Set default layout */
        if (isset($config['layout'])) {
            $this->layout = ucfirst($config['layout']);
            unset($config['layout']);
        }

        /** Set urlManager */
        if (isset($config['urlManager']) && $config['urlManager']) {
            $this->router->add($config['urlManager']);
            unset($config['urlManager']);
        } else {
            throw new \Exception('urlManager not found in config file');
        }

        /** Set charset */
        if (isset($config['charset'])) {
            $this->charset = ucfirst($config['charset']);
            unset($config['charset']);
        }

        /** Set charset */
        if (isset($config['language'])) {
            $this->language = ucfirst($config['language']);
            unset($config['language']);
        }

        /** Set other cfg */
        if (!empty($config)) {
            $this->configs = $config;
            unset($config);
        }

    }

    /**
     * Run Application
     */
    public function run()
    {
        $this->router->parsing();
        /** Prepare controller and action */
        Connect::setInstance($this);
        /** Database */
        $this->controllerInit();
        /** Build  */

        if (!DEBUG)
            unset($this->router);
    }

    /**
     * @return array
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @return bool
     */
    private function controllerInit()
    {
        if (file_exists(APP_PATH . '/controllers/' . $this->router->controller . PHP_E)) {
            include APP_PATH . '/controllers/' . $this->router->controller . PHP_E;
            $controllerName = 'controllers\\' . $this->router->controller;
            /** @var $controller Controller */
            $controller = new $controllerName;
            $action = 'action' . $this->router->action;

            if ($controller->hasMethod($action)) {
                $controller->action($this->router->action);
                return true;
            }
        }
        new \Exception('Not find controller or action');
    }
}