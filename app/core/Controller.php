<?php

namespace core;


/**
 * Class Controller
 * @package core
 */
class Controller extends Object
{
    private $view;

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * Before action
     */
    public function beforeAction()
    {
    }

    /**
     * After action
     */
    public function afterAction()
    {
    }

    /**
     * Default action
     * @param $action
     */
    public function action($action)
    {
        $this->beforeAction();
        $action = 'action' . ucfirst($action);
        $this->$action();
        $this->afterAction();
    }

    /**
     * @param $view
     * @param array $params
     */
    public function render($view, array $params = [])
    {
        $content = $this->view->render($view, $params);
        return $this->renderLayout($content);
    }

    /**
     * @param $content
     */
    public function renderLayout($content)
    {
        $layoutFile = $this->findLayoutFile();

        if ($layoutFile !== false) {
            echo $this->view->renderPhpFile($layoutFile, ['content' => $content]);
        } else {
            echo $content;
        }

    }

    /**
     * @param $urn
     */
    public function redirect($urn)
    {
        header('Location: http://' . $_SERVER['HTTP_HOST'] . $urn);
    }

    /**
     * @return bool|string
     */
    public function findLayoutFile()
    {
        $layout = App::$app->getLayout();
        $path = VIEWS_PATH . 'layout/' . strtolower($layout) . '.php';

        if (file_exists($path)) {
            return $path;
        }
        return false;
    }


}