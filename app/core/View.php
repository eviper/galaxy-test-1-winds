<?php

namespace core;

/**
 * Class View
 * @package core
 */
class View
{
    use Assets;

    public $title;
    public $assetBundles = [];

    public function __construct()
    {
        $this->title = App::$app->getName();
    }

    /**
     * @param $view
     * @param array $params
     * @return string
     */
    public function render($view, array $params = [])
    {
        $viewFile = $this->findViewFile($view);
        return $this->renderPhpFile($viewFile, $params);
    }

    /**
     * @param $view
     * @return string
     * @throws \Exception
     */
    public function findViewFile($view)
    {
        $path = VIEWS_PATH . strtolower(App::$app->router->controller) . '/' . $view . '.php';
        if (file_exists($path)) {
            return $path;
        } else {
            throw new \Exception('View file not found ' . $path);
        }
    }

    /**
     * @param $_file_
     * @param array $_params_
     * @return string
     */
    public function renderPhpFile($_file_, $_params_ = [])
    {

        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        include($_file_);
        return ob_get_clean();
    }

    /**
     * Marks the ending of a page.
     */
    public function endPage()
    {
        ob_end_flush();
    }
	
}