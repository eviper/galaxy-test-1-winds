<?php

namespace core\db;

/**
 * Class Db
 * @package core\db
 */
class Db
{
    /** @var $sth \PDOStatement */
    public static $statement;

    /**
     * @param string $sql
     * @param string $class
     * @return array
     * @throws \Exception
     */
    public static function query($sql = '', $class = '')
    {
        self::checkSQL($sql);

        $sth = self::prepare($sql);

        $res = $sth->execute();
        if ($res !== false && $class) {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        } elseif ($res !== false) {
            return $sth->fetchAll();
        } else {
            return array();
        }
    }

    /**
     * @param string $sql
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public static function execute($sql = '', array $params)
    {
        self::checkSQL($sql);
        self::$statement = self::prepare($sql);
        $res = self::$statement->execute($params);
        return $res;
    }

    /**
     * @param string $sql
     * @param array $params
     * @param string $class
     * @return bool|mixed
     * @throws \Exception
     */
    public static function getModel($sql = '', array $params = array(), $class = '')
    {

        if (self::execute($sql, $params)) {
            return self::$statement->fetchObject($class);
        }

        return false;
    }


    /**
     * @param string $sql
     * @return bool
     * @throws \Exception
     */
    public static function remove($sql = '')
    {
        self::checkSQL($sql);
        self::$statement = self::prepare($sql);
        $res = self::$statement->execute();
        return $res;
    }

    /**
     * @param $sql
     * @return \PDOStatement
     */
    public static function prepare($sql)
    {
        return Connect::$pdo->prepare($sql);
    }


    /**
     * @return array
     */
    public static function getErrors()
    {
        return Connect::$pdo->errorInfo();
    }

    /**
     * @param $sql
     * @throws \Exception
     */
    private static function checkSQL($sql)
    {
        if (empty($sql)) {
            throw new \Exception('Send an empty sql query');
        }
    }
}