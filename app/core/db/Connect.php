<?php

namespace core\db;

use core\AppInterface;

/**
 * Class Connect
 * @package core\db
 */
class Connect
{
    /** @var $pdo \PDO */
    public static $pdo;

    /**
     * Create singleton class Connect
     * @param AppInterface $app
     */
    public static function setInstance(AppInterface $app)
    {
        if (self::$pdo === null) {
            new Connect($app);
        }
    }

    /**
     * Connect constructor.
     * @param AppInterface $app
     */
    private function __construct(AppInterface $app)
    {
        $db = $app->getDb();
        try {
            self::$pdo = new \PDO($db['dsn'], $db['username'], $db['password']);
            self::$pdo->exec("set names " . $db['charset']);
        } catch (\PDOException $exception) {
            echo 'Нет соединения с базой данных';
        }
    }
}