<?php

namespace core;

/**
 * Class Html
 * @package core
 */
class Html
{
    /**
     * @param $text
     * @param $link
     * @param array $options
     * @return string
     */
    public static function a($text, $link, array $options = [])
    {
        return '<a href="' . $link . '" ' . self::renderOptions($options) . '>' . $text . '</a>';
    }

    /**
     * @param array $options
     * @return string
     */
    public static function renderOptions(array $options = [])
    {
        $attributes = '';
        foreach ($options as $attr => $option) {
            $attributes .= $attr . '="' . $option . '" ';
        }
        return $attributes;
    }

    /**
     * @param $url
     * @param $active
     * @return string
     */
    public static function activeMark($url, $active = 'active')
    {
        if ($_SERVER['REQUEST_URI'] == $url) {
            return ' ' . $active;
        }
        return '';
    }

}