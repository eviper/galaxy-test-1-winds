<?php

namespace core;


class Object
{

    /** @var array $properties */
    protected static $properties = [];

    /** @var array $listProperties */
    protected $listProperties = [];

    /**
     * @param $name
     * @return bool
     */
    public static function hasMethod($name)
    {
        $controller = (new \ReflectionClass(static::className()));
        if ($controller->getMethod($name))
            return $controller->getMethod($name)->class == static::className();
        return false;
    }


    /**
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * Get list current properties
     * @return array
     */
    public static function properties()
    {
        if (empty(static::$properties)) {
            $refClass = new \ReflectionClass(static::class);
            foreach ($refClass->getProperties() as $property) {
                if ($property->class == $refClass->name)
                    static::$properties[] = $property->name;
            }
        }
        return static::$properties;
    }

    public static function classCurrentName()
    {
        return substr(strrchr(static::className(), "\\"), 1);
    }

    /**
     * Get list current properties
     * @return array
     */
    public function listProperty()
    {
        foreach (static::properties() as $property) {
            $this->listProperties[$property] = $this->$property;
        }
        return $this->listProperties;
    }

}