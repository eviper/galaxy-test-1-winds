<?php

namespace core\console;

use core\AppInterface;
use core\db\Connect;

/**
 * Class Console
 * @package core
 */
class App implements AppInterface
{
    public static $app;

    public $migratePath;

    public $migrateNameSpace;

    private $db;

    /**
     * Console constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct($config = [])
    {

        if (!self::$app)
            self::$app = $this;
        /** Set db */
        if (isset($config['db']) && is_array($config['db'])) {
            $this->db = $config['db'];
            unset($config['db']);
        } else {
            throw new \Exception('Not found db config ');
        }
        /** Set migratePath */
        if (isset($config['migratePath'])) {
            $this->migratePath = $config['migratePath'];
            unset($config['migratePath']);
        } else {
            $this->migratePath = APP_PATH . '/migrate';
        }
        /** Set migratePath */
        if (isset($config['migrateNameSpace'])) {
            $this->migrateNameSpace = $config['migrateNameSpace'];
            unset($config['migrateNameSpace']);
        } else {
            $this->migrateNameSpace = 'migrate';
        }

    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }

    public function run()
    {
        Connect::setInstance($this);
        $this->command();
    }

    private function command()
    {
        if (!empty($_SERVER['argv'])) {
            echo '  ' . $_SERVER['argv'][1] . "\n";
            if ($_SERVER['argv'][1] == 'migrate' || $_SERVER['argv'][1] == 'migrate/up') {
                $migrate = new Migrate();
                $migrate->runUp();
            }
            if ($_SERVER['argv'][1] == 'migrate/down') {
                $migrate = new Migrate();
                $migrate->runDown();
            }
        }
    }


}