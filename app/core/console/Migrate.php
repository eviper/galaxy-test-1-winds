<?php

namespace core\console;


use core\db\Db;

class Migrate
{

    public function up()
    {
    }

    public function down()
    {
    }

    public function runUp()
    {
        foreach ($this->getMigrates() as $migrate) {
            $migrate->up();
        }
    }

    public function runDown()
    {
        foreach ($this->getMigrates() as $migrate) {
            $migrate->down();
        }
    }


    /**
     * @return Migrate[]
     */
    public function getMigrates()
    {
        $migrates = [];
        $migrateFiles = glob(App::$app->migratePath . '/*_migrate.php');
        foreach ($migrateFiles as $migrateFile) {
            $file = pathinfo($migrateFile);
            $class = App::$app->migrateNameSpace . '\\' . $file['filename'];
            $migrates[] = new $class;
        }

        return $migrates;
    }


    /**
     * @param $table
     * @param $columns
     */
    public function createTable($table, $columns)
    {
        $time = $this->beginCommand("create table $table");
        $sql = 'CREATE TABLE ' . $table . " (\n" . implode(",\n", $columns) . "\n)";
        Db::$statement = Db::prepare($sql);
        Db::$statement->execute();
        $this->endCommand($time);
    }

    public function dropTable($table)
    {
        $time = $this->beginCommand("drop table $table");
        $sql = 'DROP TABLE ' . $table;
        Db::$statement = Db::prepare($sql);
        Db::$statement->execute();
        $this->endCommand($time);
    }

    public function addPrimaryKey($name, $table, $columns)
    {
        $time = $this->beginCommand("add primary key $name on $table (" . (is_array($columns) ? implode(',', $columns) : $columns) . ')');
        $sql = 'ALTER TABLE ' . $table . ' ADD PRIMARY KEY (' . (is_array($columns) ? implode(',\n', $columns) : $columns) . ');';
        Db::$statement = Db::prepare($sql);
        Db::$statement->execute();
        $this->endCommand($time);
    }

    public function createIndex($name, $table, $columns, $unique = null)
    {
        $time = $this->beginCommand('create' . " index $name on $table (" . implode(',', (array)$columns) . ')');
        $sql = 'CREATE INDEX ' . (($unique) ? 'UNIQUE' : '') . '`' . $name . '`' . ' ON ' . $table . ' (' . (is_array($columns) ? implode(',\n', $columns) : $columns) . ');';
        Db::$statement = Db::prepare($sql);
        Db::$statement->execute();
        echo $sql;
        $this->endCommand($time);
    }


    protected function beginCommand($description)
    {
        echo "    > $description ...";
        return microtime(true);
    }


    protected function endCommand($time)
    {
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }


}