<?php

namespace helpers;

/**
 * Class HtmlHelper
 * @package components
 */
class HtmlHelper
{

    /**
     * @param $text
     * @param string $name
     */
    public static function print_pre($text, $name = '')
    {
        $id = uniqid();
        echo '<p><a data-toggle="collapse" href="#' . $id . '"><b>' . $name . '</b></a></p>';
        echo '<pre  id="' . $id . '" class="collapse in">' . $text . '</pre>';
    }

    public static function printArrayTree($arr = [], $name = '')
    {
        $path = [0];
        $result = [];

        foreach ($arr as $item) {
            if (!in_array($item['parent'], $path)) {
                $path[] = $item['parent'];
            } elseif ($item['parent'] !== end($path)) {
                while ($path && $item['parent'] !== end($path))
                    array_pop($path);
            }
            $deep = count($path);

            $result[] = ['value' => $item['value'], 'deep' => $deep];

        }
        self::renderTree($result, $name);
    }


    private static function renderTree($array, $name = 'Tree')
    {
        $space = '   ';
        $startSymbol = '-&gt;';
        $id = uniqid();
        echo '<p><a data-toggle="collapse" href="#' . $id . '"><b>' . $name . '</b></a></p>';
        echo '<pre  id="' . $id . '" class="collapse in">';
        foreach ($array as $item) {
            $spaceSize = '';
            for ($i = 0; $i < $item['deep']; $i++) {
                $spaceSize .= $space;
            }
            $value = $item['value'];
            if ($item['deep'] !== 1) {
                $value = $startSymbol . $value;
            }
            echo $spaceSize . $value . '</br>';
        }
        echo '</pre>';
    }

    public static function renderMatrix($array, $name = 'Tree')
    {
        $id = uniqid();
        echo '<p><a data-toggle="collapse" href="#' . $id . '"><b>' . $name . '</b></a></p>';
        echo '<pre  id="' . $id . '" class="collapse in">';
        foreach ($array as $items) {

            foreach ($items as $item) {
                echo ' ' . $item;
            }

            echo '</br>';
        }
        echo '</pre>';
    }

    private function __construct()
    {
    }
}