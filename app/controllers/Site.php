<?php

namespace controllers;

use components\config\GenerateConfig;
use components\CustomParser;
use components\GenerateRandomMatrix;
use components\GeneratorRandomArrayTree;
use components\lib\RandomCharacter;
use components\MatrixAllWay;
use core\Controller;
use models\Category;

/**
 * Class Site
 * @package controllers
 */
class Site extends Controller
{

    /**
     * Display All tasks
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }


    /**
     * Display Test 1
     */
    public function actionTest1()
    {
        $text = 'На десятом месте — [PHONE:phone model]HTC U Ultra[/PHONE].
        Смартфон получил большой [DISPLAY:display size]5,7-дюймовый[/DISPLAY] дисплей,
        над которым расположен второй, [INFO]вспомогательный экран. На нем появляется основная информация — время,
        погода, заряд батареи и уведомления приложений.[/INFO][DISPLAY:display size]5,7-дюймовый[/DISPLAY][INFO]вспомогательный экран[/INFO]
        Задняя крышка новинки выполнена из стекла с эффектом переливающейся поверхности.';

        $customParser = new CustomParser($text);
        $matches = $customParser->parseText();
        return $this->render('test/1', [
            'customParser' => $customParser,
            'matches' => $matches
        ]);
    }


    /**
     * Display Test 2
     */
    public function actionTest2()
    {
        $generatorConfig = new GenerateConfig([
            'deep' => 5,
            'width' => 5,
            'char_max' => 5,
            'char_min' => 2,
            'child' => 2,
            'widthStatic' => true
        ]);
        $generator = new GeneratorRandomArrayTree($generatorConfig);
        Category::populate($generator->getArray());
        return $this->render('test/2', [
            'generator' => $generator
        ]);
    }

    /**
     * Display Test 3
     */
    public function actionTest3()
    {
        $generator = new GenerateRandomMatrix(4, 5, new RandomCharacter(RandomCharacter::MAX_NUMBER));
        $matrixAllWay = new MatrixAllWay($generator->getArray());
        return $this->render('test/3', [
            'generator' => $generator,
            'matrixAllWay' => $matrixAllWay
        ]);
    }


}