<?php

use helpers\HtmlHelper;
use models\Category;

/** @var $this \core\View */
/** @var $category Category */
/** @var $generator \components\GeneratorRandomArrayTree */

$this->title = 'Test 2';

?>

<h2><?= $this->title ?></h2>

<pre>Дана таблица в базе MySQL с полями:
    id  - ключ
    name  имя,
    parent ссылка на id родителя,

Данную таблицу нужно заполнить рандомными значениями, но так что бы получилось дерево с несколькими (от 0 до 5) уровнями вложенности

Реализовать алгоритм выводящий это дерево, вида:
EEE
  -&gt;KK
  -&gt;LK
RE
LO
  -&gt;EW
  -&gt;FS
DF
  -&gt;JJJ
	  -&gt;WW
	  -&gt;LL
		-&gt;SS
		  -&gt;SD
		  -&gt;HR
			-&gt;JS
			  -&gt;PP
			-&gt;ET
  -&gt;ED
  -&gt;AC
PPP
и т.д.
Ниже из таблицы из задания 3 сделать выборку записей без потомков, но с 2-мя старшими родителями (используется SQL запрос, не циклы)</pre>

<br/>
<h3>Решение</h3>
<h5>Запрос на выборку записей без потомков, но с 2-мя старшими родителями</h5>
<pre>SELECT cat2.id, cat2.name, cat2.parent FROM category
    INNER JOIN category as cat1 on cat1.parent = category.id
    INNER JOIN category	as cat2 on cat2.parent = cat1.id
    LEFT JOIN category as catChild ON cat2.id = catChild.parent

    WHERE catChild.id IS NULL;
</pre>

<h5>Результат</h5>

<table class="table">
    <thead>
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>PARENT</th>
    <tr>
    </thead>
    <tbody>
    <?php foreach (Category::getCustomFields() as $category): ?>
        <tr>
            <td><?= $category->id ?></td>
            <td><?= $category->name ?></td>
            <td><?= $category->parent ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php HtmlHelper::printArrayTree($generator->getArray(), 'Tree'); ?>


