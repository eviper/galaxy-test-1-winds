<?php

use helpers\HtmlHelper;

/** @var $this \core\View */
/** @var $generator \components\GenerateRandomMatrix */
/** @var $matrixAllWay \components\MatrixAllWay */
$this->title = 'Test 3';
?>

<h2><?= $this->title ?></h2>
<p>Дан 2-х мерный массив, количество элементов в каждой строке может быть разной и заранее не известно. Так же не
    известно количество строк.
    Нужно разработать алгоритм, на выходе которого получим массив, в котром будет представлены все возможные уникальные
    комбинации вариантов использующий по одному элементу из каждой строки.</p>
<p>Пример</p>
<p>Исходный массив:</p>
<p>а1 а2 а3</p>
<p>b1 b2</p>
<p>c1 c2 c3</p>
<p>d1</p>
...
<br/>
<h3>Решение</h3>
<?php
HtmlHelper::renderMatrix($generator->getArray(), 'Исходные данные');
HtmlHelper::renderMatrix($matrixAllWay->getArray(), 'Результат');
?>

