<?php

use helpers\HtmlHelper;

/** @var $this \core\View */
/** @var $customParser \components\CustomParser */
/** @var $matches [] */
$this->title = 'Test 1';
?>

    <h2><?= $this->title ?></h2>
    <p>Дан текст с включенными в него тегами следующего вида:</p>
    <p>[НАИМЕНОВАНИЕ_ТЕГА:описание]данные[/НАИМЕНОВАНИЕ_ТЕГА]</p>
    <p>На выходе нужно получить 2 массива:</p>
    <p>Первый:</p>
    <p>* Ключ - наименование тега</p>
    <p>* Значение - данные</p>
    <p>Второй:</p>
    <p>* Ключ - наименование тега</p>
    <p>* Значение - описание</p>
    <br/>
    <p>Вложенность тегов не допускается.</p>
    <p>Описания может и не быть</p>
    <p>Обезателен закрвающий тег</p>

    <br/>
    <h3>Решение</h3>
    <h4>Текст</h4>
    <p>Паттерн: <?= $customParser->pattern ?></p>
    <pre><?= $customParser->text ?></pre>
<?php

HtmlHelper::print_pre(print_r($customParser->parseText(), true), 'matches [ ]');

$first = [];
$second = [];

foreach ($matches as $match) {
    $first[] = [
        'key' => $match[1],
        'data' => $match[3]
    ];
    $second[] = [
        'key' => $match[1],
        'data' => ($match[2]) ?? ''
    ];
}

HtmlHelper::print_pre(print_r($first, true), 'first [ ]');
HtmlHelper::print_pre(print_r($second, true), 'second [ ]');
?>