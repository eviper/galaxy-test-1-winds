<?php
/**
 * Control panel
 * @var $this \core\View
 */

use core\Html;

?>

<div class="nav flex-column nav-pills" aria-orientation="vertical">
    <?= Html::a('Главная', '/', ['class' => 'nav-item nav-link' . Html::activeMark('/')]); ?>
    <?= Html::a('Test 1', '/test/1', ['class' => 'nav-item nav-link' . Html::activeMark('/test/1')]); ?>
    <?= Html::a('Test 2', '/test/2', ['class' => 'nav-item nav-link' . Html::activeMark('/test/2')]); ?>
    <?= Html::a('Test 3', '/test/3', ['class' => 'nav-item nav-link' . Html::activeMark('/test/3')]); ?>
</div>
