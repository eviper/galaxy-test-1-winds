<?php
/* @var $this \core\View */

/* @var $content string */

use core\App;

?>
    <!doctype html>
    <html lang="<?= App::$app->language ?>">
    <head>
        <meta charset="<?= App::$app->charset ?>">

        <title><?= $this->title ?></title>
        <?php $this->js('jquery-3.2.1.min.js') ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <!-- Bootstrap stylesheet -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <!-- Bootstrap scripts -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

        <?php $this->css('style.css'); ?>
    </head>
    <body>
    <div class="row">
        <div class="col-md-2">
            <div class="menu">
                <header class="header_menu">
                    <h4>Меню</h4>
                </header>
                <section>
                    <?= $this->render('../layout/_menu') ?>
                </section>
            </div>
        </div>
        <div class="col-md-8">

            <?= $content ?>

        </div>
    </div>
    </body>
    </html>
<?php $this->endPage() ?>