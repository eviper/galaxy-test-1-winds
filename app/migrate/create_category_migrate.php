<?php

namespace migrate;

use core\console\Migrate;

class create_category_migrate extends Migrate
{
    public function up()
    {
        $this->createTable(
            'category',
            array(
                'id INT(5) NOT NULL',
                'name varchar(255) NOT NULL',
                'parent int(5) NOT NULL'
            )
        );
        $this->addPrimaryKey('pk-category-id', 'category', 'id');
        $this->createIndex('idx-category-parent', 'category', 'parent');
    }

    public function down()
    {
        $this->dropTable('category');
    }
}