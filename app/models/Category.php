<?php

namespace models;

use core\db\Db;
use core\Model;

/**
 * Class Category
 * @package models
 *
 * @property integer $id
 * @property integer $parent
 * @property string $name
 *
 */
class Category extends Model
{
    public $id;
    public $parent;
    public $name;

    public static function tableName()
    {
        return 'category';
    }

    /**
     * Populate Category
     * @param $arrayTree
     */
    public static function populate($arrayTree)
    {
        self::truncate();
        if (!self::execute($arrayTree))
            print_r(Db::getErrors());

    }

    public static function truncate()
    {
        $sql = "TRUNCATE " . self::tableName() . ";";
        Db::$statement = Db::prepare($sql);
        Db::$statement->execute();
    }

    public static function execute($arrayTree)
    {
        $columns = '(id, name, parent)';
        $valuesArr = [];
        foreach ($arrayTree as $item) {
            $valuesArr[] = [$item['id'], $item['value'], $item['parent']];
        }

        $values = '';
        foreach ($valuesArr as $value) {
            $values .= '("' . implode('","', $value) . '"),';
        }
        $values = substr($values, 0, -1);

        $sql = "INSERT INTO " . self::tableName() . $columns . " VALUES " . $values . ";";

        Db::$statement = Db::prepare($sql);
        return Db::$statement->execute();
    }


    /**
     * @return mixed
     */
    public static function getCustomFields()
    {
        $sql = "SELECT cat2.id, cat2.name, cat2.parent FROM category
            INNER JOIN category as cat1 on cat1.parent = category.id
            INNER JOIN category	as cat2 on cat2.parent = cat1.id
            LEFT JOIN category as catChild ON cat2.id = catChild.parent
        
            WHERE catChild.id IS NULL;";
        Db::$statement = Db::prepare($sql);
        Db::$statement->execute();
        return Db::$statement->fetchAll(
            \PDO::FETCH_CLASS,
            Category::className()
        );
    }
}