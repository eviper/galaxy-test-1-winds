<?php
defined('APP_PATH') or define('APP_PATH', __DIR__);

function __autoload($class)
{
    include APP_PATH . '/' . str_replace('\\', '/', $class) . '.php';
}