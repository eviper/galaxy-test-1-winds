<?php
defined('DEBUG') or define('DEBUG', true);
defined('PUBLIC_PATH') or define('PUBLIC_PATH', __DIR__);

$config = require(__DIR__ . '/../config/web.php');
require(PUBLIC_PATH . '/../app/autoload.php');

(new core\App($config))->run();
